#!/usr/bin/python3

import sys
import pygame
import time
import glob
from collections import namedtuple

def err (fmt, *a, **d):
    raise RuntimeError(fmt.format(*a, **d))


dmsg_list = []
def dmsg (fmt, *a, **b):
    global dmsg_list
    msg = fmt.format(*a, **b)
    print(msg)
    dmsg_list.append(msg);
    return

def nop ():
    return

EMPTY_SET = frozenset()

EMPTY = 0
NORMAL_FLOOR = 1
WALL = 2
EXIT = 3
LEVEL = 100

WALKABLE = frozenset((NORMAL_FLOOR,EXIT))

HERO = 0
CARD = 1
DOOR = 2
COIN = 3

OBJECT_NAME = {HERO: 'hero', CARD: 'card', DOOR: 'door', COIN: 'coin'}

NORTH = 0
WEST = 1
SOUTH = 2
EAST = 3
NO_DIR = -1

CLOSED = 0
OPENING = 1
OPEN = 2
CLOSING = 3



def direction_from_string (s):
    s = s.lower()
    if s == 'north': return NORTH
    if s == 'south': return SOUTH
    if s == 'west': return WEST
    if s == 'east': return EAST
    return NO_DIR

DIR_STR = ('north', 'west', 'south', 'east')

ROW_DELTA = {NORTH: -1, SOUTH: 1, WEST: 0, EAST: 0}
COL_DELTA = {NORTH: 0, SOUTH: 0, WEST: -1, EAST: 1}

STAY = 0
MOVE = 1
TURN = 2
SHRINK = 3

CTL_UP = NORTH
CTL_LEFT = WEST
CTL_DOWN = SOUTH
CTL_RIGHT = EAST

BLACK = 0
BLUE = 1
GREEN = 2
CYAN = 3
RED = 4
MAGENTA = 5
YELLOW = 6
GREY = 7
BASIC_COLORS = 8

def color_from_string (s):
    return dict(black=BLACK,blue=BLUE,green=GREEN,cyan=CYAN,red=RED,magenta=MAGENTA,yellow=YELLOW,grey=GREY,gray=GREY)[s.lower()]

def color_code_to_rgb (color_code, intensity):
    r = (color_code >> 2) & 1
    g = (color_code >> 1) & 1
    b = color_code & 1
    return (r * intensity, g * intensity, b * intensity)

def wh_cmp (a, b):
    if a[0] == b[0] and a[1] == b[1]: return 0
    if a[0] > b[0] or a[1] > b[1]: return 1
    return -1

def wh_text_size (text, size, a = 6, b = 80):
    while a < b:
        c = (a + b) >> 1
        font = pygame.font.Font(None, c)
        text_surface = font.render(text, False, (0, 0, 0))
        cmp_result = wh_cmp(text_surface.get_size(), size)
        if cmp_result < 0: a = c + 1
        else: b = c - 1
    return c

def wh_text (text, size, color, a = 6, b = 80):
    font_size = wh_text_size(text, size, a, b)
    font = pygame.font.Font(None, font_size)
    return font.render(text, False, color)


LegendItem = namedtuple('LegendItem', 'kw attr')

class LevelState (object):

    def __init__ (ls, path):
        object.__init__(ls)
        ls.view_r = -1
        ls.view_c = -1
        ls.load(path)
        ls.trans_obj_list = [[] for i in range(ls.move_ticks)] # list of object transitioning
        ls.trans_steps = max(ls.move_ticks, ls.turn_ticks)
        ls.open_close_ticks = ls.move_ticks * 2 // 3
        ls.trans_obj_list_index = 0
        ls.finished = False
        return

    def adjust_view (ls, r, c):
        # update view_r/view_c so that hero is visible
        if r <= ls.view_r: ls.view_r -= ls.height - 1
        elif r >= ls.view_r + ls.height: ls.view_r += ls.height - 1
        if c <= ls.view_c: ls.view_c -= ls.width - 1
        elif c >= ls.view_c + ls.width: ls.view_c += ls.width - 1
        return

    def load (ls, path):
        with open(path, 'rt') as f: lines = f.readlines()
        ls.map_text = []
        ls.legend = dict()
        ls.legend[' '] = LegendItem('nop', dict())
        section = '-'
        ls.tile_map = []
        ls.tile_obj = []

        for x in lines:
            if x.startswith('#'): continue
            x = x.rstrip()
            if x.startswith('[') and x.endswith(']'):
                section = x[1:len(x) - 1]
                continue
            getattr(ls, 'load_{}_line'.format(section))(x)

        for i in range(len(ls.map_text)):
            for j in range(0, len(ls.map_text[i]), 2):
                ch = ls.map_text[i][j]
                li = ls.legend[ch]
                getattr(ls, 'tile_init_{}'.format(li.kw))(i, j >> 1, **li.attr)

        for i in range(len(ls.map_text)):
            for j in range(1, len(ls.map_text[i]), 2):
                ch = ls.map_text[i][j]
                li = ls.legend[ch]
                getattr(ls, 'tile_init_{}'.format(li.kw))(i, j >> 1, **li.attr)
        dmsg('playing {}', path)
        return

    def load_map_line (ls, line):
        dmsg('map: {}', line)
        ls.map_text.append(line)
        return

    def load_legend_line (ls, legend):
        if legend == '': return
        ch = legend[0]
        d = dict()
        kw = None
        for seg in legend[2:].split(' '):
            if not kw:
                kw = seg
                continue
            sp = seg.split('=', 1)
            key = sp[0]
            val = sp[1] if len(sp) == 2 else True
            d[key] = val
        ls.legend[ch] = LegendItem(kw, d)
        dmsg('legend: "{}" -> {} {}', ch, kw, ' '.join('{}={}'.format(k, d[k]) for k in d))
        return

    def load_ui_line (ls, kv):
        if kv == '': return
        (k, v) = kv.split(None, 1)
        if k == 'width': ls.width = int(v)
        elif k == 'height': ls.height = int(v)
        elif k == 'lpanel': ls.lpanel = 0 #int(v)
        elif k == 'fps': ls.fps = int(v)
        elif k == 'move_ticks': ls.move_ticks = int(v)
        elif k == 'turn_ticks': ls.turn_ticks = int(v)
        elif k == 'top': ls.view_r = int(v)
        elif k == 'left': ls.view_c = int(v)
        else: raise RuntimeError("unknown ui key '{}'".format(k))
        return

    def set_tile (ls, i, j, val):
        while i >= len(ls.tile_map):
            ls.tile_map.append(bytearray())
        row = ls.tile_map[i]
        if j >= len(row):
            row.extend(bytearray(j - len(row) + 1))
        row[j] = val
        return

    def get_tile (ls, i, j, dflt=0):
        if i < 0 or j < 0: return dflt
        if i >= len(ls.tile_map): return dflt
        if j >= len(ls.tile_map[i]): return dflt
        return ls.tile_map[i][j]

    def add_tile_object (ls, i, j, o):
        while i >= len(ls.tile_obj): ls.tile_obj.append([])
        row = ls.tile_obj[i]
        while j >= len(row): row.append(set())
        row[j].add(o)
        return

    def del_tile_object (ls, i, j, o):
        ls.tile_obj[i][j].discard(o)
        return

    def get_tile_objects (ls, i, j, dflt=EMPTY_SET):
        if i < 0 or j < 0: return dflt
        if i >= len(ls.tile_obj): return dflt
        if j >= len(ls.tile_obj[i]): return dflt
        return ls.tile_obj[i][j]

    def tile_init_nop (ls, i, j, **a):
        return

    def tile_init_floor (ls, i, j, **a):
        ls.set_tile(i, j, 1)
        return

    def tile_init_wall (ls, i, j, **a):
        ls.set_tile(i, j, 2)
        return

    def tile_init_hero (ls, i, j, direction='east', **a):
        ls.hero = Hero(i, j, direction_from_string(direction))
        ls.add_tile_object(i, j, ls.hero)
        dmsg('hero {}:{}', i, j)
        return

    def tile_init_exit (ls, i, j, **a):
        ls.set_tile(i, j, EXIT)
        return

    def tile_init_card (ls, i, j, color='red', **a):
        card = AccessCard(i, j, color_from_string(color))
        ls.add_tile_object(i, j, card)
        return

    def tile_init_door (ls, i, j, color='grey', dir1='', dir2='', **a):
        door = Door(i, j, color_from_string(color), direction_from_string(dir1), direction_from_string(dir2))
        ls.add_tile_object(i, j, door)
        return

    def tile_init_coin (ls, i, j, color='grey', value='1', **a):
        value = int(value)
        coin = Coin(i, j, color_from_string(color), value)
        ls.add_tile_object(i, j, coin)
        return

    def tile_init_level (ls, i, j, digit, **a):
        t = ls.get_tile(i, j)
        if t == EMPTY: t = 0
        else: t -= LEVEL
        t = t * 10 + int(digit)
        dmsg('level {} at {}x{}', t, i, j)
        ls.set_tile(i, j, LEVEL + t)
        return

    def add_obj_to_trans_list (ls, obj, steps):
        index = (ls.trans_obj_list_index + steps) % ls.trans_steps
        ls.trans_obj_list[index].append(obj)
        return

    def tick_tock (ls):
        ls.trans_obj_list_index = (ls.trans_obj_list_index + 1) % ls.move_ticks
        for i in range(ls.move_ticks):
            idx = (i + ls.trans_obj_list_index) % ls.move_ticks
            for obj in ls.trans_obj_list[idx]:
                obj.tick_tock(ls)
        del ls.trans_obj_list[ls.trans_obj_list_index][:]
        return ls.finished

    def control (ls, ctl):
        if ctl == CTL_UP: ls.hero.move(NORTH, ls)
        elif ctl == CTL_DOWN: ls.hero.move(SOUTH, ls)
        elif ctl == CTL_LEFT: ls.hero.move(WEST, ls)
        elif ctl == CTL_RIGHT: ls.hero.move(EAST, ls)
        return

    pass # LevelState


class LevelObject (object):

    def __init__ (obj, r, c, d = NORTH):
        obj.row = r
        obj.col = c
        obj.action = STAY
        obj.clear_trans()
        obj.direction = d
        obj.target_tile = None
        return

    def clear_trans (obj):
        obj.step = 0
        obj.dir_delta = 0
        obj.row_delta = 0
        obj.col_delta = 0
        return

    def turn (obj, direction, level):
        if obj.action != STAY: return
        if direction == obj.direction: return
        obj.action = TURN
        obj.dir_delta = (direction - obj.direction) & 3
        if obj.dir_delta == 3: obj.dir_delta = -1
        level.add_obj_to_trans_list(obj, level.turn_ticks)
        return

    def move (obj, direction, level):
        if obj.action != STAY: return
        if obj.direction != direction:
            obj.turn(direction, level)
            return
        new_row = obj.row + ROW_DELTA[direction]
        new_col = obj.col + COL_DELTA[direction]
        target_tile = level.get_tile(new_row, new_col)
        if target_tile in WALKABLE:
            # validate we can leave all objects in the current tile
            for item in level.get_tile_objects(obj.row, obj.col):
                if item is obj: continue
                method = 'can_leave_{}'.format(OBJECT_NAME[item.KIND])
                if not hasattr(obj, method): return
                if not getattr(obj, method)(item, level): return

            # validate we can enter over all objects in the target tile
            for item in level.get_tile_objects(new_row, new_col):
                method = 'can_enter_over_{}'.format(OBJECT_NAME[item.KIND])
                if not hasattr(obj, method): return
                if not getattr(obj, method)(item, level): return

            # interact with objects leaving behind
            for item in level.get_tile_objects(obj.row, obj.col):
                if item is obj: continue
                method = 'leave_{}'.format(OBJECT_NAME[item.KIND])
                if not hasattr(obj, method): return
                getattr(obj, method)(item, level)

            # interact with objects in the new tile
            for item in level.get_tile_objects(new_row, new_col):
                method = 'enter_over_{}'.format(OBJECT_NAME[item.KIND])
                if not hasattr(obj, method): return
                getattr(obj, method)(item, level)

            obj.action = MOVE
            obj.target_tile = target_tile
            obj.row_delta = ROW_DELTA[direction]
            obj.col_delta = COL_DELTA[direction]
            level.add_obj_to_trans_list(obj, level.move_ticks)
        return

    def shrink (obj, level):
        obj.action = SHRINK
        level.add_obj_to_trans_list(obj, level.move_ticks)
        return

    def tick_tock (obj, level):
        assert obj.action != STAY
        obj.step += 1
        if obj.action == MOVE and obj.step == level.move_ticks // 2 + 1:
            level.adjust_view(obj.row + obj.row_delta, obj.col + obj.col_delta)
            return

        if obj.action == MOVE and obj.step == level.move_ticks:
            obj.action = STAY
            level.del_tile_object(obj.row, obj.col, obj)
            obj.row += obj.row_delta
            obj.col += obj.col_delta
            level.add_tile_object(obj.row, obj.col, obj)
            obj.clear_trans()
            dmsg("obj finished transition {}:{}:{}", obj.row, obj.col, obj.direction)
            if obj.target_tile == EXIT:
                level.finished = True
                level.level_jump = 1
            obj.target_tile = None
            return

        if obj.action == TURN and obj.step == level.turn_ticks:
            obj.action = STAY
            obj.direction = (obj.direction + obj.dir_delta) & 3
            obj.clear_trans()
            return

        if obj.action == SHRINK and obj.step == level.move_ticks:
            level.del_tile_object(obj.row, obj.col, obj)
            return
        return

    pass # LevelObject

class Hero (LevelObject):
    KIND = HERO
    def __init__ (hero, r, c, d, money=0):
        LevelObject.__init__(hero, r, c, d)
        hero.access = [None] * BASIC_COLORS
        hero.access[GREY] = True
        hero.money = money
        return

    def can_enter_over_card (hero, card, level):
        return True

    def enter_over_card (hero, card, level):
        card.shrink(level)
        hero.access[card.color] = True
        return

    def can_enter_over_door (hero, door, level):
        return hero.direction in door.directions and hero.access[door.color]

    def enter_over_door (hero, door, level):
        door.open(level)
        return

    def can_leave_door (hero, door, level):
        return hero.direction in door.directions and hero.access[door.color]

    def leave_door (hero, door, level):
        door.close(level)
        return

    def can_enter_over_coin (hero, coin, level):
        return True

    def enter_over_coin (hero, coin, level):
        coin.shrink(level)
        hero.money += coin.value
        dmsg('hero money: {}', hero.money)
        return

# class Hero

class AccessCard (LevelObject):
    KIND = CARD
    def __init__ (card, r, c, color=RED):
        LevelObject.__init__(card, r, c, NO_DIR)
        card.color = color
        return
    pass # AccessCard

class Coin (LevelObject):
    KIND = COIN
    def __init__ (coin, row, col, color_code=GREY, value=5):
        LevelObject.__init__(coin, row, col, NO_DIR)
        coin.color_code = color_code
        coin.value = value
        return
# class Coin


class Door (LevelObject):
    KIND = DOOR

    def __init__ (door, row, col, color=GREY, dir1=EAST, dir2=NO_DIR):
        if dir1 not in (NORTH, SOUTH, WEST, EAST): err('dir1={}', dir1)
        if (dir1 in (WEST, EAST) and dir2 in (NORTH, SOUTH)) or (dir1 in (NORTH, SOUTH) and dir2 in (WEST, EAST)):
            err('bad door dir: dir1={} dir2={}', DIR_STR[dir1], DIR_STR[dir2])
        LevelObject.__init__(door, row, col)
        door.directions = (dir1,) if dir2 == NO_DIR else (dir1, dir2)
        door.state = CLOSED
        door.color = color
        return

    def open (door, level):
        door.state = OPENING
        door.step = 0
        dmsg('opening door at {}:{}', door.row, door.col)
        level.add_obj_to_trans_list(door, level.open_close_ticks)
        return

    def close (door, level):
        door.state = CLOSING
        door.step = 0
        dmsg('closing door at {}:{}', door.row, door.col)
        level.add_obj_to_trans_list(door, level.open_close_ticks)
        return

    def tick_tock (door, level):
        door.step += 1
        if door.state == OPENING and door.step >= level.open_close_ticks:
            door.state = OPEN
            dmsg('door open at {}:{}', door.row, door.col)
            return
        if door.state == CLOSING and door.step >= level.open_close_ticks:
            door.state = CLOSED
            dmsg('door closed at {}:{}', door.row, door.col)
            return
        return

    pass # Door


class GameState (object):
    def __init__ (game):
        with open('levels.txt', 'rt') as f:
            game.level_list = [ line.strip() for line in f.readlines() ]

        try:
            with open('settings.txt', 'rt') as f: lines = f.readlines()
            for x in lines:
                if x.startswith('#'): continue
                x = x.rstrip()

        except IOError:
            game.player = None

        game.running = True
        game.level_index = 0
        return
    def get_current_level (game):
        level = LevelState('{}.level.txt'.format(game.level_list[game.level_index]))
        return level
    def level_exited (game, level):
        if not level.finished:
            game.running = False
        else:
            game.level_index = (game.level_index + level.level_jump) % len(game.level_list)
        return
# class Game

###############################################################################
# ui stuff below

BLACK_COLOR = (0, 0, 0)

def gen_rotations (surface, n):
    a = [surface]
    for i in range(1, n):
        a.append(pygame.transform.rotate(surface, i * 360 // n))
    return a

def linear_shrink (surface, n):
    a = [surface]
    w = surface.get_width()
    h = surface.get_height()
    for i in range(n - 1, 0, -1):
        a.append(pygame.transform.smoothscale(surface, (w * i // n, h * i // n)))
    return a

class LevelUI (object):

    def __init__ (ui, gui, state):
        object.__init__(ui)

        ui.gui = gui
        ui.state = state

        ui.lpanel_bg = (0, 0, 32)
        ui.floor_bg = (32, 32, 32)
        ui.floor_fg = (64, 64, 64)
        ui.wall_color = (128, 128, 128)
        ui.cards = [ None ] * 8
        ui.ctl_pressed = None
        ui.init()
        return

    def init (ui):
        state = ui.state
        surface = ui.gui.surface
        w = surface.get_width()
        h = surface.get_height()
        max_tile_width = w // (state.width + state.lpanel)
        max_tile_height = h // (state.height + 1)
        ui.tile_width = min(max_tile_height, max_tile_width)
        ui.tile_height = ui.tile_width
        dmsg('tile: {}x{}', ui.tile_width, ui.tile_height)
        ui.lpanel_x = (w - (state.width + state.lpanel) * ui.tile_width) >> 1
        ui.lpanel_y = (h - state.height * ui.tile_height) >> 1
        ui.lpanel_w = state.lpanel * ui.tile_width
        ui.lpanel_h = state.height * ui.tile_height
        ui.lpanel_rect = (ui.lpanel_x, ui.lpanel_y, ui.lpanel_w, ui.lpanel_h)
        dmsg('lpanel: {}/{}', ui.lpanel_x, ui.lpanel_y)
        ui.map_x = ui.lpanel_x + state.lpanel * ui.tile_width
        ui.map_y = ui.lpanel_y
        ui.map_w = state.width * ui.tile_width
        ui.map_h = state.height * ui.tile_height
        dmsg('map: {}/{}', ui.map_x, ui.map_y)
        ui.wipe = []
        if ui.lpanel_x > 0: ui.wipe.append((0, 0, ui.lpanel_x, h))
        x = ui.lpanel_x + ui.lpanel_w + ui.map_w
        if x < w: ui.wipe.append((x, 0, w - x, h))
        if ui.lpanel_y > 0: ui.wipe.append((ui.lpanel_x, 0, ui.lpanel_w + ui.map_w, ui.lpanel_y))
        y = ui.map_y + ui.map_h
        if y < h: ui.wipe.append((ui.lpanel_x, y, ui.lpanel_w + ui.map_w, h - y))

        ui.tile_size = (ui.tile_width, ui.tile_height)
        ui.tile_model_rect = (0, 0, ui.tile_width, ui.tile_height)

        ui.prepare_hero()
        ui.prepare_exit_tile()

        ui.move_dir = None
        return

    def prepare_exit_tile (ui):
        surface = pygame.Surface(ui.tile_size)
        surface.fill(ui.floor_bg)
        sign_rect = pygame.Rect(ui.tile_width // 8, ui.tile_height // 4, ui.tile_width * 6 // 8, ui.tile_height * 2 // 4)
        pygame.draw.rect(surface, (0, 128, 0), sign_rect, 0)
        text_rect = pygame.Rect(sign_rect.left + 2, sign_rect.top + 2, sign_rect.width - 4, sign_rect.height - 4)
        text = wh_text('EXIT', text_rect.size, (200, 200, 100))
        # a = 6
        # b = 80
        # while a < b:
        #     c = (a + b) >> 1
        #     font = pygame.font.Font(None, c)
        #     text = font.render('EXIT', False, (200, 200, 100))
        #     cmp_result = wh_cmp(text.get_size(), text_rect.size)
        #     if cmp_result < 0: a = c + 1
        #     else: b = c - 1
        surface.blit(text, ((ui.tile_width - text.get_width()) >> 1, (ui.tile_height - text.get_height()) >> 1))
        sign_rect.top -= 1
        sign_rect.left -= 1
        sign_rect.bottom += 1
        sign_rect.right += 1
        pygame.draw.rect(surface, (128, 128, 128), sign_rect, 1)
        ui.exit_surface = surface
        return

    def prepare_hero (ui):
        surface = pygame.Surface(ui.tile_size)
        surface.fill(BLACK_COLOR)
        surface.set_colorkey(BLACK_COLOR)
        x = ui.tile_width // 8
        y = ui.tile_height // 8
        w = ui.tile_width - 2 * x
        h = ui.tile_height - 2 * y
        pygame.draw.ellipse(surface, (128, 128, 0), (x, y, w, h), 0)
        x = ui.tile_width // 2
        y = ui.tile_height // 2
        pygame.draw.line(surface, (196, 196, 128), (x, 2), (x, y), 2)
        ui.rot_hero = gen_rotations(surface, ui.state.turn_ticks * 4)
        return

    def prepare_card (ui, color):
        if ui.cards[color]: return ui.cards[color]
        w = ui.tile_width * 3 // 5
        h = ui.tile_height * 2 // 5
        surface = pygame.Surface((w, h))
        c1 = color_code_to_rgb(color, 128)
        c2 = color_code_to_rgb(color, 192)
        surface.fill(c1)
        pygame.draw.rect(surface, c2, (0, 0, w, h), 1)
        pygame.draw.line(surface, c2, (w // 8, h // 3), (w, h // 3), 2)
        sl = linear_shrink(surface, ui.state.move_ticks)
        ui.cards[color] = sl
        return sl

    def select_hero_surface (ui, hero):
        idx = (hero.direction * ui.state.turn_ticks + hero.step * hero.dir_delta) % (ui.state.turn_ticks * 4)
        return ui.rot_hero[idx]

    def run (ui):
        ui.gui.run(ui, ui.state.fps)
        return

    def process_event (ui, e):
        if e.type == pygame.KEYDOWN:
            if e.key == pygame.K_ESCAPE or e.key == pygame.K_q: return True
            elif e.key == pygame.K_F9:
                ui.gui.show_dmsg = not ui.gui.show_dmsg
            elif e.key == pygame.K_F11:
                ui.gui.toggle_fullscreen()
                ui.init()
            elif e.key == pygame.K_F7:
                ui.state.level_jump = -1
                ui.state.finished = True
            elif e.key == pygame.K_F8:
                ui.state.level_jump = +1
                ui.state.finished = True
            elif e.key == pygame.K_DOWN or e.key == pygame.K_j: ui.ctl_pressed = CTL_DOWN
            elif e.key == pygame.K_UP or e.key == pygame.K_k: ui.ctl_pressed = CTL_UP
            elif e.key == pygame.K_LEFT or e.key == pygame.K_h: ui.ctl_pressed = CTL_LEFT
            elif e.key == pygame.K_RIGHT or e.key == pygame.K_l: ui.ctl_pressed = CTL_RIGHT
            dmsg('pressed ctl {}', ui.ctl_pressed)
        elif e.type == pygame.KEYUP:
            ctl_released = None
            if e.key == pygame.K_DOWN or e.key == pygame.K_j: ctl_released = CTL_DOWN
            elif e.key == pygame.K_UP or e.key == pygame.K_k: ctl_released = CTL_UP
            elif e.key == pygame.K_LEFT or e.key == pygame.K_h: ctl_released = CTL_LEFT
            elif e.key == pygame.K_RIGHT or e.key == pygame.K_l: ctl_released = CTL_RIGHT
            if ui.ctl_pressed == ctl_released: ui.ctl_pressed = None
            dmsg('released ctl {}', ui.ctl_pressed)
        elif e.type == pygame.VIDEORESIZE:
            dmsg('video resize: {}x{}', e.w, e.h)
            ui.gui.set_mode(e.w, e.h, ui.gui.req_flags)
            ui.init()
        return

    def rect (ui, quad, col, width=0):
        pygame.draw.rect(ui.gui.surface, col, quad, width)
        return

    def get_tile_top_left (ui, i, j):
        x = ui.map_x + j * ui.tile_width - (ui.tile_width >> 1)
        y = ui.map_y + i * ui.tile_height - (ui.tile_height >> 1)
        return (x, y)

    def get_tile_center (ui, i, j):
        x = ui.map_x + j * ui.tile_width
        y = ui.map_y + i * ui.tile_height
        return (x, y)

    def draw_map (ui):
        level = ui.state
        for r in range(-1, level.height + 1):
            for c in range(-1, level.width + 1):
                t = level.get_tile(level.view_r + r, level.view_c + c)
                (x, y) = ui.get_tile_top_left(r, c)
                tile_rect = (x, y, ui.tile_width, ui.tile_height)
                if t == EMPTY:
                    ui.rect(tile_rect, (0, 0, 0))
                elif t == NORMAL_FLOOR:
                    ui.rect(tile_rect, ui.floor_bg)
                elif t == WALL:
                    ui.rect(tile_rect, ui.wall_color)
                elif t == EXIT:
                    ui.gui.surface.blit(ui.exit_surface, (x, y))
                elif t >= LEVEL:
                    t -= LEVEL
                    ui.rect(tile_rect, (64 + 16 * t // 10, 64 + 16 * (t % 10), 0))
        for r in range(-1, level.height + 1):
            for c in range(-1, level.width + 1):
                t = level.get_tile(level.view_r + r, level.view_c + c)
                (x, y) = ui.get_tile_top_left(r, c)
                tile_edge_rect = (x, y, ui.tile_width + 1, ui.tile_height + 1)
                if t != EMPTY:
                    ui.rect(tile_edge_rect, ui.floor_fg, 1)
        return

    def draw_hero (ui, hero, x, y):
        level = ui.state
        hs = ui.select_hero_surface(hero)
        (w, h) = hs.get_size()
        x += hero.col_delta * ui.tile_width * hero.step // ui.state.move_ticks
        y += hero.row_delta * ui.tile_height * hero.step // ui.state.move_ticks
        ui.gui.surface.blit(hs, (x - (w >> 1), y - (h >> 1)))
        return

    def draw_card (ui, card, x, y):
        if card.step == ui.state.move_ticks: return
        surface_list = ui.prepare_card(card.color)
        surface = surface_list[card.step]
        (w, h) = surface.get_size()
        x -= w >> 1
        y -= h >> 1
        ui.gui.surface.blit(surface, (x, y))
        return

    def draw_coin (ui, coin, x, y):
        if coin.step == ui.state.move_ticks: return
        r = min(ui.tile_width, ui.tile_height) * (ui.state.move_ticks - coin.step) // (4 * ui.state.move_ticks)
        color1 = color_code_to_rgb(coin.color_code, 128)
        color2 = color_code_to_rgb(coin.color_code, 192)
        pygame.draw.circle(ui.gui.surface, color1, (x, y), r)
        pygame.draw.circle(ui.gui.surface, color2, (x, y), r, 3)
        return

    def draw_door (ui, door, x, y):
        n = ui.state.open_close_ticks
        if door.state == CLOSED: m = n
        elif door.state == OPEN: m = 0
        elif door.state == OPENING: m = n - door.step
        elif door.state == CLOSING: m = door.step
        color1 = color_code_to_rgb(door.color, 96)
        color2 = color_code_to_rgb(door.color, 128)
        w = ui.tile_width
        h = ui.tile_height
        xb = x - w // 2 + 1
        yb = y - h // 2 + 1
        xe = xb + w - 1
        ye = yb + h - 1
        if door.directions[0] in (WEST, EAST):
            x0 = xb + 3 * w // 8
            x1 = xe - 3 * w // 8
            hh = h // 8 + m * 3 * h // (8 * n)
            pygame.draw.rect(ui.gui.surface, color1, (x0, yb, x1 - x0, hh))
            pygame.draw.rect(ui.gui.surface, color1, (x0, ye - hh, x1 - x0, hh))
            pygame.draw.rect(ui.gui.surface, color2, (x0, yb, x1 - x0, hh), 1)
            pygame.draw.rect(ui.gui.surface, color2, (x0, ye -hh, x1 - x0, hh), 1)
            x2 = xb + 1 * w // 8
            x3 = xe - 1 * w // 8
            hh2 = h // 8
            pygame.draw.rect(ui.gui.surface, color1, (x2, yb, x3 - x2, hh2))
            pygame.draw.rect(ui.gui.surface, color1, (x2, ye - hh2, x3 - x2, hh2))
            pygame.draw.rect(ui.gui.surface, color2, (x2, yb, x3 - x2, hh2), 1)
            pygame.draw.rect(ui.gui.surface, color2, (x2, ye - hh2, x3 - x2, hh2), 1)
        else:
            y0 = yb + 3 * h // 8
            y1 = ye - 3 * h // 8
            ww = w // 8 + m * 3 * w // (8 * n)
            pygame.draw.rect(ui.gui.surface, color1, (xb, y0, ww, y1 - y0))
            pygame.draw.rect(ui.gui.surface, color1, (xe - ww, y0, ww, y1 - y0))
            pygame.draw.rect(ui.gui.surface, color2, (xb, y0, ww, y1 - y0), 1)
            pygame.draw.rect(ui.gui.surface, color2, (xe -ww, y0, ww, y1 - y0), 1)
            y2 = yb + 1 * h // 8
            y3 = ye - 1 * h // 8
            ww2 = w // 8
            pygame.draw.rect(ui.gui.surface, color1, (xb, y2, ww2, y3 - y2))
            pygame.draw.rect(ui.gui.surface, color1, (xe - ww2, y2, ww2, y3 - y2))
            pygame.draw.rect(ui.gui.surface, color2, (xb, y2, ww2, y3 - y2), 1)
            pygame.draw.rect(ui.gui.surface, color2, (xe - ww2, y2, ww2, y3 - y2), 1)

        return

    def draw_objects (ui):
        level = ui.state
        y = ui.map_y - ui.tile_height

        for r in range(-1, level.height + 1):
            x = ui.map_x - ui.tile_width
            for c in range(-1, level.width + 1):
                for o in level.get_tile_objects(level.view_r + r, level.view_c + c):
                    if not o: continue
                    draw_met = 'draw_{}'.format(OBJECT_NAME[o.KIND])
                    if hasattr(ui, draw_met):
                        getattr(ui, draw_met)(o, x, y)
                    else:
                        pygame.draw.circle(ui.gui.surface, (128,0,0), (x, y), ui.tile_width // 4)
                x += ui.tile_width
            y += ui.tile_height
        return


    def draw (ui):
        ui.draw_map()
        ui.draw_objects()
        #ui.rect(ui.lpanel_rect, ui.lpanel_bg)
        for r in ui.wipe:
            ui.rect(r, (0, 0, 0))
        return

    def tick_tock (ui, n):
        if n % 100 == 0: dmsg('tick={}', n)
        if ui.ctl_pressed != None:
            ui.state.control(ui.ctl_pressed)
        return ui.state.tick_tock()

    pass # LevelUI

#* MainGUI ******************************************************************
class MainGUI (object):

    def __init__ (gui):
        object.__init__(gui)
        pygame.init()
        gui.show_dmsg = True
        gui.set_mode(1500, 1000, pygame.HWSURFACE | pygame.DOUBLEBUF | pygame.RESIZABLE)
        gui.width = gui.surface.get_width()
        gui.height = gui.surface.get_height()
        gui.background = pygame.Surface(gui.surface.get_size()).convert()
        gui.background.fill((0, 0, 0));
        gui.dmsg_font = pygame.font.Font(None, 18)
        dmsg("screen: {}x{}", gui.width, gui.height)
        return

    def set_mode (gui, w, h, flags):
        gui.req_width = w
        gui.req_height = h
        gui.req_flags = flags
        gui.surface = pygame.display.set_mode((w, h), flags)
        return

    def toggle_fullscreen (gui):
        if gui.req_flags & pygame.FULLSCREEN:
            gui.set_mode(800, 600, pygame.HWSURFACE | pygame.DOUBLEBUF | pygame.RESIZABLE)
        else:
            gui.set_mode(0, 0, pygame.HWSURFACE | pygame.DOUBLEBUF | pygame.FULLSCREEN)
        return

    def run (gui, ui, fps=30):
        clock = pygame.time.Clock()
        #pygame.time.set_timer(pygame.USEREVENT, 1000//fps)
        n = 0
        while True:
            for e in pygame.event.get():
                dmsg('event: {}', pygame.event.event_name(e.type))
                if ui.process_event(e): return
            if (ui.tick_tock(n)): return
            ui.draw()
            gui.draw_dmsg()
            pygame.display.flip()
            pygame.time.wait(0)
            #time.sleep(1.0 / fps)
            sl = clock.tick(fps)
            #dmsg("slept {}", sl)
            #pygame.time.wait(1000//fps)
            #pygame.time.wait(1000)
            n += 1
        return

    def draw_dmsg (gui):
        global dmsg_list
        if not gui.show_dmsg: return
        y = gui.height
        for line in reversed(dmsg_list):
            text = gui.dmsg_font.render(line, False, (0, 128, 0))
            y -= text.get_height()
            if y < 0: break
            gui.surface.blit(text, (0, y))
        return
    def process_event (gui, event):
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_ESCAPE or event.key == pygame.K_q:
                return True
        return

    def clear (gui, surface):
        surface.blit(gui.background, (0, 0))
        return

    def draw (gui):
        gui.clear(gui.surface)
        return

    def tick_tock (gui, n):
        if n % 10 == 0: dmsg('tick={}', n)
        return

    def run_game (game_ui, game):
        while game.running:
            level = game.get_current_level()
            level_ui = LevelUI(game_ui, level)
            level_ui.run()
            game.level_exited(level)

    pass # GameUI


def main ():
    game = GameState()
    gui = MainGUI()
    gui.run_game(game)
    return

if __name__ == '__main__': main()

